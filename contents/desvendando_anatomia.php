<div id="background1">
    <div class="row mt-4">
        <div class="col-1 col-md-2"></div>
        <div class="col-10 col-md-8 text-center column-overflow">

            <div class="d-none d-md-block mt-5"></div>

            <span class="font-title largeText pageTitle">
                Tenha mais <u>segurança nos seus procedimentos injetáveis</u>, elimine o medo de errar e conquiste mais reconhecimento profissional.
            </span>

            <div class="mt-3 normaText">
                As vagas para a Formação US Dermo Inject serão abertas. Assista o vídeo abaixo e entenda.
            </div>

            <div class="mt-3">
                <lite-youtube videoid="0HPteD_GxMU" class="videoBlock">
                    <a class="lty-playbtn" title="Play Video">
                        <span class="lyt-visually-hidden">Play Video: Keynote (Google I/O '18)</span>
                    </a>
                </lite-youtube>
            </div>

            <div class="mt-2 font-gold normaText textPreCadastrio">
                Faça seu pré-cadastro gratuito clicando abaixo:
            </div>

            <div class="mt-2">
                <span class="greyBg bigText">
                    EXCLUSIVO PARA MÉDICOS
                </span>
            </div>

            <div class="mt-3">
                <span>
                    <i class="fa fa-calendar-alt"></i>
                </span>
                DIA 21 DE MARÇO, <u>ÀS 20H00</u>
            </div>

            <button class="btn btn-block btn-inscricao mt-3 position-relative" id="btnParticipar">
                QUERO FAZER MINHA PRÉ-INSCRIÇÃO AGORA
                <i class="far fa-arrow-alt-circle-right iconButton"></i>
            </button>

        </div>
        <div class="col-1 col-md-2"></div>
    </div>
    <div class="mt-5">

    </div>
    <br>
</div>

<div id="background2">
    <div class="d-none d-md-block mt-5"></div>
    <div class="d-md-none mt-2"></div>
    <div class="row mb-5">
        <div class="col-2 col-md-2 col-lg-1"></div>

        <div class="col-8 col-md-3 col-lg-2 d-flex justif-content-between align-items-center">
            <img src="./assets/LOGO-US.svg" alt="" class="card-img">
            <img src="./assets/logoAlaska.webp" alt="" class="card-img">
        </div>

        <div class="d-block d-md-none col-2"></div>

        <div class="col-1 col-md-1 col-lg-3"></div>

        <div class="d-none col-md-5 d-md-flex justify-content-between align-items-center smallText">
            CNPJ: 30.337.874/0001-08
            <a href="https://usdermo.com/termos-de-uso" class="links">
                Políticas de privacidade
            </a>
            <a href="https://usdermo.com/termos-de-uso" class="links">
                Termos de uso
            </a>
        </div>

        <div class="d-md-none col-10 smallText text-center">
            CNPJ: 30.337.874/0001-08 <br>
            <a href="https://usdermo.com/termos-de-uso" class="links">
                Políticas de privacidade
            </a> <br>
            <a href="https://usdermo.com/termos-de-uso" class="links">
                Termos de uso
            </a>
        </div>

        <div class="col-1 col-md-1"></div>
    </div>
</div>