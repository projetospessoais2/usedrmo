<?php 
    $utmSource = !empty($_GET["utm_source"]) ? $_GET["utm_source"] : null;
    $utmMedium = !empty($_GET["utm_medium"]) ? $_GET["utm_medium"] : null;
    $utmCampaign = !empty($_GET["utm_campaign"]) ? $_GET["utm_campaign"] : null;
    $utmTerm = !empty($_GET["utm_term"]) ? $_GET["utm_term"] : null;
    $utmContent = !empty($_GET["utm_content"]) ? $_GET["utm_content"] : null;
?>

<div class="modal">
    <div class="modal_content">
        <span class="close">&times;</span>
        
        <div class="row mt-1">
            <div class="col-1"></div>
            <div class="col-10 text-center titleModal">
                Preencha o formulário para fazer sua pré inscrição:
            </div>
            <div class="col-1"></div>
        </div>

        <form method="POST" id="whForm" class="_form _form_51 _inline-form  _dark" novalidate data-styles-version="4">

            <input type="text" id="nome" name="nome" class="form-control inputFormActive mt-4" placeholder="Seu Nome Completo">
            <input type="email" id="email" name="email" class="form-control inputFormActive mt-3" placeholder="Seu Melhor E-mail">
            <input type="phone" id="cel" name="cel" class="form-control inputFormActive mt-3" placeholder="Seu WhatsApp com DDD" pattern="^(\(11\) [9][0-9]{4}-[0-9]{4})|(\(1[2-9]\) [5-9][0-9]{3}-[0-9]{4})|(\([2-9][1-9]\) [5-9][0-9]{3}-[0-9]{4})$">
            <input type="text" id="UFCRM" name="UFCRM" class="form-control inputFormActive mt-3" placeholder="UF/CRM">

            <!-- UTMS -->
            <input type="hidden" id="utm_source" name="utm_source" value="<?= $utmSource ?>" placeholder=""/>
            <input type="hidden" id="utm_medium" name="utm_medium" value="<?= $utmMedium ?>" placeholder=""/>
            <input type="hidden" id="utm_campaign" name="utm_campaign" value="<?= $utmCampaign ?>" placeholder=""/>
            <input type="hidden" id="utm_term" name="utm_term" value="<?= $utmTerm ?>" placeholder=""/>
            <input type="hidden" id="utm_content" name="utm_content" value="<?= $utmContent ?>" placeholder=""/>

            <!-- DATA -->
            <input type="hidden" id="field[15]" name="field[15]" value="<?= $todaysDate ?>" placeholder=""/>

            <div class="text-center mt-4 mb-3">
                <button type="button" id="sendForm" onclick="sendWebhook()" class="btn btn-block btn-inscricao noAnimation font-weight-bolder">ENVIAR</button>
            </div>
        </form>
        <div class="legendaModal text-center">
            Após o cadastro, <span class="brownBg"> NÃO FECHE A PÁGINA</span> que abrirá! Tem uma informação importantíssima nela!
        </div>


    </div>
</div>

<script>
    
const buttons = document.querySelectorAll("#btnParticipar")
const modal = document.querySelector(".modal")
const closeBtn = document.querySelector(".close")

document.addEventListener("DOMContentLoaded",() => {
    buttons.forEach((button) => {
        button.addEventListener("click", handleClick);
    });
})

function handleClick(event) {
  modal.style.display = "block";
  closeBtn.addEventListener("click", () => {
    modal.style.display = "none"
  })
}

function sendWebhook() {
    var request = new XMLHttpRequest();
    request.open("POST", "https://n8n.alaskalancamentos.online/webhook/5b5db707-3e00-47dc-954a-7b55bf9205f1-firmiano");

    // request.setRequestHeader('Content-type', 'application/json; charset=utf-8');

    var params = {
        "fields[nome][value]": document.getElementById('nome').value,
        "fields[email][value]": document.getElementById('email').value,
        "fields[cel][value]": document.getElementById('cel').value,
        "fields[cel][raw_value]": document.getElementById('cel').value,
        "fields[UFCRM][value]": document.getElementById('UFCRM').value,
        "fields[utm_source][value]": document.getElementById('utm_source').value,
        "fields[utm_medium][value]": document.getElementById('utm_medium').value,
        "fields[utm_campaign][value]": document.getElementById('utm_campaign').value,
        "fields[utm_term][value]": document.getElementById('utm_term').value,
        "fields[utm_content][value]": document.getElementById('utm_content').value
    }

    request.onreadystatechange = function() {//Call a function when the state changes.
        if(request.readyState == 4 && request.status == 200) {
            window.location.href = "https://usdermo.com/obrigado-desvendando-a-anatomia-periorbitaria/"
        }
    }

    request.send(JSON.stringify(params));
}
</script>