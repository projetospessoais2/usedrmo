<!DOCTYPE html>
<html lang="pt_BR">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="initial-scale=1" />

        <title>Desvendando a Anatomia Periorbitária – 21 de Março – Fernanda Cavallieri</title>

        <link rel="icon" type="image/x-icon" href="./assets/favicon.ico">
        <!-- <link rel="stylesheet" href="./vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="./css/fonts.css">
        <link rel="stylesheet" href="./css/style.css">
        <link rel="stylesheet" href="./css/form.css">
        <link rel="stylesheet" href="./vendor/lite-youtube-embed/lite-yt-embed.css">
        
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" /> -->

        <link rel="stylesheet" href="./css/used.css">

        <link rel="stylesheet" href="./vendor/lite-youtube-embed/lite-yt-embed.css">
        <script src="./vendor/lite-youtube-embed/lite-yt-embed.js"></script>

        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MHSQW78');</script>
        <!-- End Google Tag Manager -->
    </head>

    <body style="background-color: #14100B">
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MHSQW78"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        
        <?php require ('./contents/desvendando_anatomia.php'); ?>
        <?php require ('./contents/form.php'); ?>
    </body>

</html>